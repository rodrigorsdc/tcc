import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def create_pml_cv_input(fname, cmin, cmax, cint, k, A, dmax, sample):
    with open(fname, "w") as f:
        f.write(cmin + " " + cmax + " " + cint + "\n\n")
        f.write(k + "\n\n")
        f.write(str(sample.shape[1]) + "\n\n")
        f.write(A + "\n\n")
        f.write(dmax + "\n\n")
        f.write(str(sample.shape[0]) + "\n\n")
        np.savetxt(f, sample, fmt="%d")

STEP = 1

bra = pd.read_csv('Bovespa_Brasil.csv', usecols=['Data', 'Último'])
uk = pd.read_csv('FTSE_100_UK.csv', usecols=['Data', 'Último'])
ind = pd.read_csv('Nifty_50_India.csv', usecols=['Data', 'Último'])
fra = pd.read_csv('CAC_40_Franca.csv', usecols=['Data', 'Último'])
jpn = pd.read_csv('Nikkei_225_Japao.csv', usecols=['Data', 'Último'])
eua = pd.read_csv('NASDAQ_EUA.csv', usecols=['Data', 'Último'])

bra.columns = ['Data', 'Brasil']
uk.columns = ['Data', 'Reino Unido']
ind.columns = ['Data', 'Índia']
fra.columns = ['Data', 'França']
jpn.columns = ['Data', 'Japão']
eua.columns = ['Data', 'EUA']

dados = pd.merge(bra, uk, on='Data')
dados = pd.merge(dados, ind , on='Data')
dados = pd.merge(dados, fra, on='Data')
dados = pd.merge(dados,jpn, on='Data')
dados = pd.merge(dados, eua, on='Data')

arr = np.zeros((dados.shape[0]-1, 6))

for i in range(dados.shape[0]-1):    
    if (dados['Brasil'][i+1] > dados['Brasil'][i]): arr[i][0] = 1
    else:                                       arr[i][0] = 0

    if (dados['Reino Unido'][i+1] > dados['Reino Unido'][i]): arr[i][1] = 1
    else:                                       arr[i][1] = 0

    if (dados['Índia'][i+1] > dados['Índia'][i]): arr[i][2] = 1
    else:                                       arr[i][2] = 0

    if (dados['França'][i+1] > dados['França'][i]): arr[i][3] = 1
    else:                                       arr[i][3] = 0

    if (dados['Japão'][i+1] > dados['Japão'][i]): arr[i][4] = 1
    else:                                       arr[i][4] = 0

    if(dados['EUA'][i+1] > dados['EUA'][i]): arr[i][5] = 1
    else:                                    arr[i][5] = 0
    

size = arr.shape[0] // STEP
data = np.zeros((size, 6))
k = 0
for i in range(size):
    for j in range(6): data[i][j] = arr[k][j]
    k += STEP

create_pml_cv_input("data_bolsas", "0.01", "2.5", "0.01", "10", "2",
                    str(data.shape[1]-1), data)



# cv = pd.read_csv('serie.csv')
# ax = cv.plot.line(x='c', y='CV(c)')
# f = ax.get_figure()
# ax.set_title("Índices de bolsas de valores, melhor regularizador",
#              fontweight='bold')
# ax.set_xlabel("Regularizador", fontweight='bold')
# ax.set_ylabel("Valor de validação cruzada", fontweight='bold')
# ax.legend().set_visible(False)
# f.set_size_inches((9, 7))
# f.savefig('serie_cv_bolsa.pdf')

# ax = dados.plot.line()
# f = ax.get_figure()
# ax.set_title("Índices de bolsas de valores", fontweight='bold')
# ax.set_xlabel("Dia", fontweight='bold')
# ax.set_ylabel("Pontos", fontweight='bold')
# f.set_size_inches((11, 7))
# dados.plot.line()
# f.savefig('serie_paises_bolsa.pdf')
