# Algoritmos para Estimação de Modelos Gráficos

### Requisitos:
    Python versão >= 3.5.2
    gcc versão >= 5.4.0
    graph_tool do Python3 versão >= 2.27
    GNU Scientific Library versão >= 2.1
    
### Para compilar:
    make

### Para rodar os algoritmos:
###### Algoritmo de máxima verossimilhança penalizada:
    ./bin/pml [in] [cons] [ncons]


*in* é o arquivo de entrada e tem o seguinte formato: 

    V  
    A
    c    
    max_neigh
    sample_size
    sample

   onde *V* é número de vértices, *A* é tamanho do alfabeto, *c* é
     o regularizador, *max_neigh* é o número máximo de vizinhos a
     ser estimador, *sample_size* é tamanho da amostra e sample é
     uma matriz de tamanho (sample_size x V) contendo a amostra.

*cons* devolve o grafo estimado conservativo e tem o seguinte formato:
    
    V
    v1 u1
    v2 u2
    v3 u3
     ...
    vn un

   onde *V* é o número de vértices e os pares *v1 u1*, *v2 u2*, *...*,
     *vn un* são arestas do grafo

*ncons* devolve o grafo estimado não-conservativo e tem
     o mesmo formato do arquivo *cons*.

###### Algoritmo de Chow-Liu__:
    ./bin/pml [in] [out]

*in* é o arquivo de entrada e tem o mesmo formato da entrada
     do algoritmo anterior, com a exceção da primeira linha, onde
     não temos o regularizador.

*out* é o grafo de tipo árvore estimado e tem o mesmo formato
     das saídas do algoritmo anterior.

###### Algoritmo de máxima verossimilhança penalizada (Validação Cruzada)
    ./bin/pml_cv [in_cv]

*in_cv* é o arquivo de entrada do algoritmo e tem o seguinte formato:
    
    c_min c_max c_interval
    k
    V
    A
    max_neigh
    sample_size
    sample

onde *c_min*, *c_max* e *c_interval* é o menor valor, o maior e o
    intervalo do regularizador a ser estimado no algoritmo, respectivamente.
    *k* é  o número de fold.

O algoritmo produzirá um arquivo chamado *in* que é
    um arquivo de entrada para o algoritmo de máxima verossimilhança
    penalizada de acordo com o valor ótimo do regularizador.

### Para simular:
###### Algoritmo de máxima verossimilhança (grafo próprio)**:
    python3 src/python/main.py pml [c] [sample_min] [sample_max]
       [sample_interval] [samples_per_size] [A] self
       [filename]

###### Algoritmo de máxima verossimilhança (grafo aleatório)**:
    python3 src/python/main.py pml [c] [sample_min] [sample_max]
       [sample_interval] [samples_per_size] [A] random [d_min]
       [d_max] [V]

###### Algoritmo de Chow-Liu (grafo próprio)**:
    python3 src/python/main.py chow-liu [sample_min]
       [sample_max] [sample_interval] [samples_per_size] [A]
       self [filename]

###### Algoritmo de Chow-Liu (grafo aleatório)**:
    python3 src/python/main.py chow-liu [sample_min]
       [sample_max] [sample_interval] [samples_per_size] [A]
       random [V]

onde,
*sample_min*, *sample_max*, *sample_interval* é número mínimo,
 máximo e o intervalo de tamanho das amostras a serem simuladas, respectivamente
     
*samples_per_size* é o número de simulações para cada tamanho de
 amostra.

*A* é tamanho do alfabeto.

*filename* é um arquivo contendo o grafo a ser simulado

*c* é o regularizador

*d_min*, *d_max* é o grau mínimo e máximo do grafo aleatório, respectivamente

*V* é o número de vértice do grafo aleatório

Ao fim de cada simulação será gerado um diretório na pasta "simulation"
 contendo todos os grafos estimados e originais, os erros para cada
 tamanho de amostra (arquivo "erros.csv"), as informações da simulação e
 a distribuição aleatória gerado (arquivo "info.txt").