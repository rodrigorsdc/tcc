(TeX-add-style-hook
 "cap-desenvolvimentos"
 (lambda ()
   (LaTeX-add-labels
    "cap:desenvolvimentos"
    "fig:graph"
    "fig-bayesian"
    "fig:sim1"
    "fig:sim2"
    "fig:sim3"
    "fig:sim4"))
 :latex)

