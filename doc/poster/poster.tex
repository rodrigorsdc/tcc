\documentclass[a0,final]{a0poster}
%%%Load packages
\usepackage{multicol} 			%3-column layout
\usepackage[left=2cm,right=2cm,bottom=0cm,top=0cm]{geometry}			%Reset margins
\usepackage{mathpazo}			%Load palatino font & pazo math
\usepackage{color}				%Needed for colour boxes & coloured text
\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amsfonts,amssymb,amsthm, bm}
\usepackage{caption}
\usepackage[pdftex]{graphicx}
\newenvironment{Figure}
               {\par\medskip\noindent\minipage{\linewidth}}
               {\endminipage\par\medskip}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}


%%%Define colours and lengths
\definecolor{headingcol}{rgb}{1,1,1}			%Colour of main title
\definecolor{boxcol}{rgb}{0.7,0.2,0.2}		%Edge-colour of box and top banner
\fboxsep=1cm							%Padding between box and text
\setlength{\columnsep}{2cm}				%Set spacing between columns

%%%Format title
\makeatletter							%Needed to include code in main file
\renewcommand\@maketitle{%
\null									%Sets position marker
{
\color{headingcol}\sffamily\VERYHuge		%Set title font and colour
\@title \par}%
\vskip 0.6em%
{
\color{white}\sffamily\large				%Set author font and colour
\lineskip .5em%
\begin{tabular}[t]{l}%
\@author
\end{tabular}\par}%
\vskip 1cm
\par
}
\makeatother



\title{Algoritmos para Estimação de Modelos de Gráficos}

\author{Rodrigo Ribeiro Santos de Carvalho \\
  Orientadora: Florencia Graciela Leonardi \\
  Universidade de São Paulo}



\begin{document}

\hspace{-3cm}								%Align with edge of page, not margin
\colorbox{boxcol}{

  %Coloured banner across top
\begin{minipage}{1189mm}					%Minipage for title contents
  \maketitle
\end{minipage}

}

\vspace{1cm}

\begin{multicols}{3}							%Use 3-column layout
\raggedcolumns							%Don't stretch contents vertically

%%%Column1
\section*{Introdução}
O modelo gráfico probabilístico é um modelo que expressa as dependências
condicionais de variáveis aleatórias através de um grafo. O campo aleatório
de Markov é a forma de grafo não-direcionado deste modelo.

Um problema de pesquisa atual é a reconstrução, a partir de amostras,
do campo aleatório de Markov. Nesse trabalho, estudamos dois
algoritmos presentes na literatura e realizamos simulações
para verificar empiricamente a consistência. E por fim, aplicamos
um dos algoritmos em uma situação com dados reais.

\section*{Definições básicas}
Sejam $G = (V, E)$ um grafo não-direcionado e $\{X_v : v \in V\}$ uma
família de variáveis aleatórias indexadas pelo conjunto de vértices $V$
e assumindo valores em um conjunto $A$ finito.
Dizemos que $\{X_v : v \in V\}$ é um \emph{campo aleatório de Markov} em relação
ao grafo G se para cada $v \in V$ existir um conjunto $ne(v) \subseteq
V \setminus \{v\}$ tal que para todo $W \subseteq V \setminus \{v\} \cup ne(v)$,

$$\mathbb{P}(X_v = a_v | X_{ne(v)} = a_{ne(v)}) = \mathbb{P}(X_v = a_v |
X_{ne(v)} = a_{ne(v)}, X_W = a_W)$$

O conjunto $ne(v)$ é chamado de vizinhança Markoviana de $v$.

Seja $X_V^{(1:n)}$ uma amostra independente de tamanho $n$ de um campo aleatório
de Markov $\{X_v : v \in V\}$. $x_v^i$ denotará a $i$-ésima observação
da variável $X_v$. Sejam $v \in V$ e $W \subseteq V \setminus
\{v\}$. O operador $N(a_v, a_W)$ conta o número de vezes que o evento
$\{x_v^i = a_v, x_W^i = a_W\}$ acontence na amostra e é definido por
  
  $$N(a_v, a_W) = \sum_{i = 1}^n = \textbf{1}{\{x_v^i = a_v, x_W^i = a_W\}} $$


\section*{Algoritmo de máxima verossimilhança penalizada}
Este algoritmo estima cada vizinhança $ne(v)$ do
grafo através do cálculo de máxima log-verossimilhança penalizada por
um fator que depende diretamente do tamanho da vizinhança candidata. O
seu estimador é, para $c > 0$

$$\widehat{ne}(v) = \argmax_{W \subseteq V \setminus \{v\}}
\{\sum_{a_v \in A}\sum_{a_W \in A^W}N(a_v, a_W)\log\widehat{p}
(a_v, a_W) - c|A|^{|W|}\log n\}$$
onde $\widehat{p}(a_v, a_W) = \widehat{\mathbb{P}}(X_v = a_v |
X_W = a_W) = \dfrac{N(a_v, a_W)}{N(a_W)}$ é o estimador de máxima
verossimilhança probabilidade condicional $\mathbb{P}(X_v = a_v |
X_W = a_W)$ e $$N(a_W) = \sum_{a_v \in A }N(a_v, a_W)$$.

\textbf{Teorema:} Para qualquer $v \in V$ e $c > 0$, temos que
$\widehat{ne}(v) \rightarrow ne(v)$ quase certamente, quando
$n \rightarrow \infty$

\section*{Algoritmo de Chow-Liu}
Em casos em que queremos estimar um grafo de dependências que
seja uma árvore há um algoritmo quadrático. Funciona
da seguinte forma:

\begin{enumerate}
\item Para todo par $X_v$ e $X_w$ de vértices, calculamos
  a \emph{medida de informação mútua} estimada:
  $$\widehat{I}(X_v, X_w) = \sum_{(a_v, a_w) \in A^2} \dfrac{N(a_v, a_w)}{n}
  [\log\dfrac{N(a_v, a_w)}{N(a_v)(N_w)} + \log n]$$

\item Aplique um algoritmo de árvore geradora máxima onde cada
  aresta $(v, w)$ tem peso $\widehat{I}(X_v, X_w)$
\end{enumerate}

\textbf{Teorema:} O algoritmo de Chow-Liu estima uma distribuição de
segunda ordem que mais se aproxima da distribuição original em
relação à divergência de Kullback-Leibler.

\columnbreak

%%%Column 2
\section*{Simulações}
Para simular, criamos um gerador de grafos e árvores, de distribuição
e de amostras aleatórios. Aqui apresentamos duas simulações, uma para
cada algoritmo. Nelas medimos o erro de subestimação, de sobrestimação
e total. No caso do algoritmo de máxima verossimilhança penalizada,
avaliamos para tamanho de amostra de 10 até 3500.

\begin{Figure}
  \centering
  \label{fig:10_pml}
  \includegraphics[width=13cm]{figuras/10_vet_Markov_original_0.pdf}
  \captionof{figure}{Grafo de 10 vértices usado para gerar
    amostras e avaliar o algoritmo de máxima verossilhança
    penalizada.}\label{fig:10_pml}

\end{Figure}

\begin{Figure}
  \centering
  \includegraphics[width=37cm]{figuras/10_vet_history_0.pdf}
  \captionof{figure}{Erros de subestimação, de sobrestimaçao e total do
  grafo da Figura 1}\label{fig:10_his_pml}
\end{Figure}

\bigskip
Para avaliar o algoritmo de Chow-Liu geramos uma árvore e simulamos para
tamanho de 10 até 5000.

\begin{Figure}
  \centering
  \includegraphics[width=13cm]{figuras/chow_25_vet_Markov_original_1.pdf}
  \captionof{figure}{Árvore de 25 vértices usado para gerar amostras
    e avaliar o algoritmo de Chow-Liu.}\label{fig:25_chow}
\end{Figure}

\columnbreak

%%%Column 3

\begin{Figure}
  \centering
  \includegraphics[width=36cm]{figuras/chow_25_vet_history_1.pdf}
  \captionof{figure}{Erros de subestimação, de sobrestimação e total
  do grafo da Figura 3}\label{fig:25_his_chow}
\end{Figure}

\section*{Aplicação}
Usamos dados de bolsas de valores de 6 países para estimar o grafo de
dependências condicionais entre os países. Coletamos índices da bolsa
de Bovespa - Brasil, NASDAQ - EUA, FTSE 100 - Reino Unido, CAC 40 -
França, Nifty 50 - Índia e Nikkei 225 - Japão a partir do dia 05 de
Janeiro de 2001 até 22 de outubro de 2018.

\begin{Figure}
  \centering
  \includegraphics[width=\linewidth]{figuras/mapa_mundi_bolsas.pdf}
  \captionof{figure}{Grafo estimado das 6 bolsas de valores}
\end{Figure}

O grafo estimado mostra que as conexões entre as bolsas de valores
estão relaciondas com a aproximação geográfica dos países.

\section*{Agredecimento}
Agradecemos ao CNPq pelo apoio financeiro durante o desenvolvimento
deste trabalho.

\vspace{0.1cm}
\hspace{2cm}
\includegraphics[width=7cm]{figuras/ime.png}
\hspace{5cm}
\includegraphics[width=15cm]{figuras/cnpq.pdf}

\nocite*

%\bibliographystyle{plain}
%\bibliography{halobib}

\end{multicols}
\end{document}
